package exercicios.CRUD.mysql.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import exercicios.CRUD.mysql.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long>{
	
	List<Customer> findByAge(int age);
	List<Customer> findByName(String name);

}
