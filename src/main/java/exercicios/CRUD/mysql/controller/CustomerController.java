package exercicios.CRUD.mysql.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import exercicios.CRUD.mysql.model.Customer;
import exercicios.CRUD.mysql.repo.CustomerRepository;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("/api")
public class CustomerController {

	@Autowired
	CustomerRepository repository;
	
	@GetMapping("/customer")
	public List<Customer> getAllCustomer(){
		List<Customer> retorno = new ArrayList<Customer>(); 
		repository.findAll().forEach(retorno::add);

		return retorno;
	}
	
	@PostMapping("/")
	public Customer insertCustomer(Customer customer){
		Customer _customer = repository.save(new Customer(customer.getName(), customer.getLastName(), customer.getAge()));
		return _customer;
	}
	
	@DeleteMapping("/del")
	public ResponseEntity<String> deleteCustomer(Long id) {
		repository.deleteById(id);
		
		return new ResponseEntity<>("Usuário deletado com sucesso!", HttpStatus.OK);
	}
	
	@PutMapping("/customers/{id}")
	public ResponseEntity<Customer> update(@PathVariable("id") Long id, @RequestBody Customer customer){
		
		Optional<Customer> op = repository.findById(id);
		
		if(op.isPresent()) {
			Customer _customer = op.get();
			_customer.setAge(customer.getAge());
			_customer.setLastName(customer.getLastName());
			_customer.setName(customer.getName());
			return new ResponseEntity<>(repository.save(_customer),HttpStatus.OK);
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
}
